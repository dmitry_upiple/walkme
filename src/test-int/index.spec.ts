import request from "supertest";
import { app } from "../index";
import products from "../products.json";
import customers from "../customers.json";

const CUSTOMERS_URL = `/customers/`;
const PRODUCTS_URL = `/products/`;
const MULTIPLE_URL = `/multiple/`;
const VALID_PRODUCT_ID = "993";
const INVALID_PRODUCT_ID = "999";
const VALID_CUSTOMER_ID = "13";
const INVALID_CUSTOMER_ID = "999";
const MULTIPLE_QUERY = '?bob=/customers/13&alice=/customers/25&ketchup=/products/993&mustard=/products/90';
const MULTIPLE_QUERY_PRODUCT_404 = '?mustard=/products/90';
const MULTIPLE_QUERY_PRODUCT_404_KEY = 'mustard';

const INVALID_PRODUCT_RESPONSE = { message: 'product does not exist' };
const INVALID_CUSTOMER_RESPONSE = { message: 'customer does not exist' };
const INVALID_MULTIPLE_QUERY_PRODUCT = {
    "mustard": {
        "error": {
            "status": 404,
            "response": {
                "message": "product does not exist"
            }
        }
    }
};
const EMPTY_RESPONSE = {};

const MULTIPLE_NORMAL_RESPONSE = {
    "bob": {
        "data": {
            "name": "Bob",
            "id": "13",
            "age": "27"
        }
    },
    "alice": {
        "data": {
            "name": "Alice",
            "id": "25",
            "age": "18"
        }
    },
    "ketchup": {
        "data": {
            "name": "Heinz",
            "id": "993",
            "price": "221.00"
        }
    },
    "mustard": {
        "error": {
            "status": 404,
            "response": {
                "message": "product does not exist"
            }
        }
    }
};
describe('Global Runner', () => {
    describe('Products API Endpoint', () => {
        // Status 200 Ok - VALID_PRODUCT_ID
        it("GET /products/:id - Success", async () => {
            const response = await request(app).get(`${PRODUCTS_URL}${VALID_PRODUCT_ID}`).send();
            const product = products.find((product) => product.id === VALID_PRODUCT_ID);
            // Should be 200 OK
            expect(response.status).toBe(200);

            // Should be equal
            expect(response.body).toEqual(product);

            // Should be defined keys
            expect(response.body.name).toBeDefined();
            expect(response.body.id).toBeDefined();
            expect(response.body.price).toBeDefined();

            // Should have same value each key
            expect(response.body.name).toEqual(product.name);
            expect(response.body.id).toEqual(product.id);
            expect(response.body.price).toEqual(product.price);

        });

        // Status 404 NotFound - INVALID_PRODUCT_ID
        it("GET /products/:id - Failed", async () => {
            const response = await request(app).get(`${PRODUCTS_URL}${INVALID_PRODUCT_ID}`).send();
            expect(response.status).toBe(404);
            expect(response.body).toEqual(INVALID_PRODUCT_RESPONSE);
        });
        // Status 404 NotFound - product was not provided
        it("GET /products/ - Failed", async () => {
            const response = await request(app).get(`${PRODUCTS_URL}`).send();
            expect(response.status).toBe(404);
            expect(response.body).toEqual(EMPTY_RESPONSE);
        });

    });

    describe('Customers API Endpoint', () => {
        // Status 200 Ok - VALID_CUSTOMER_ID
        it("GET /customers/:id - Success", async () => {
            const response = await request(app).get(`${CUSTOMERS_URL}${VALID_CUSTOMER_ID}`).send();
            const customer = customers.find((customer) => customer.id === VALID_CUSTOMER_ID);
            // Should be 200 OK
            expect(response.status).toBe(200);

            // Should be equal
            expect(response.body).toEqual(customer);

            // Should be defined keys
            expect(response.body.name).toBeDefined();
            expect(response.body.id).toBeDefined();
            expect(response.body.age).toBeDefined();

            // Should have same value each key
            expect(response.body.name).toEqual(customer.name);
            expect(response.body.id).toEqual(customer.id);
            expect(response.body.age).toEqual(customer.age);

        });

        // Status 404 NotFound - INVALID_CUSTOMER_ID
        it("GET /products/:id - Failed", async () => {
            const response = await request(app).get(`${CUSTOMERS_URL}${INVALID_CUSTOMER_ID}`).send();
            expect(response.status).toBe(404);
            expect(response.body).toEqual(INVALID_CUSTOMER_RESPONSE);
        });
        // Status 404 NotFound - customer was not provided
        it("GET /products/ - Failed", async () => {
            const response = await request(app).get(`${PRODUCTS_URL}`).send();
            expect(response.status).toBe(404);
            expect(response.body).toEqual(EMPTY_RESPONSE);
        });
    });

    describe('Multiple API Endpoint', () => {
        // Status 200 Ok - with MULTIPLE_QUERY
        it("GET /multiple/?bob=/customers/13&alice=/customers/25&ketchup=/products/993&mustard=/products/90 - Success", async () => {
            const response = await request(app).get(`${MULTIPLE_URL}${MULTIPLE_QUERY}`).send();
            expect(response.status).toBe(200);
            expect(response.body).toEqual(MULTIPLE_NORMAL_RESPONSE);
        });
        // Status 200 Ok - with MULTIPLE_QUERY_404
        it("GET /multiple/?mustard=/products/90 - Success", async () => {
            const response = await request(app).get(`${MULTIPLE_URL}${MULTIPLE_QUERY_PRODUCT_404}`).send();
            expect(response.status).toBe(200);
            expect(response.body).toEqual(INVALID_MULTIPLE_QUERY_PRODUCT);
            expect(response.body[MULTIPLE_QUERY_PRODUCT_404_KEY]).toBeDefined();
            expect(response.body[MULTIPLE_QUERY_PRODUCT_404_KEY].error).toBeDefined();
            expect(response.body[MULTIPLE_QUERY_PRODUCT_404_KEY].error.status).toBeDefined();
            expect(response.body[MULTIPLE_QUERY_PRODUCT_404_KEY].error.status).toEqual(404);
            expect(response.body[MULTIPLE_QUERY_PRODUCT_404_KEY].error.response).toBeDefined();
            expect(response.body[MULTIPLE_QUERY_PRODUCT_404_KEY].error.response).toEqual(INVALID_PRODUCT_RESPONSE);
            expect(response.body[MULTIPLE_QUERY_PRODUCT_404_KEY].error.response.message).toEqual("product does not exist");
        });

        // Empty response
        it("GET /multiple/ - Failed", async () => {
            const response = await request(app).get(`${MULTIPLE_URL}`).send();
            expect(response.status).toBe(200);
            expect(response.body).toEqual(EMPTY_RESPONSE);
        });

    });
});
