import express from "express";
import * as bodyParser from "body-parser";
import * as http from "http";
import products from "./products.json";
import customers from "./customers.json";

export const app = express();

const PORT = 8000;
app.use(bodyParser.json({limit: '5000mb'}));
app.use(bodyParser.urlencoded({extended: true}));


const router = express.Router();

router.get(`/customers/:id`, async (req, res) => {
    const result = customers.find((item) => item.id === req?.params?.id);
    if (!result) {
        res.status(404).send({
            message: "customer does not exist",
        });
    }
    res.send(result);
});

router.get(`/products/:id`, async (req, res) => {
    const result = products.find((item) => item.id === req?.params?.id);
    if (!result) {
        res.status(404).send({
            message: "product does not exist",
        });
    }
    res.send(result);
});

router.get(`/multiple`, async (req, res) => {
    const keys = Object.keys(req?.query);
    res.setHeader('content-type', 'application/json');
    res.write('{');
    let i = 1;
    for (const key of keys) {
        if (req?.query[key]) {
            res.write(`"${key}" : `);
            await new Promise((resolve, reject) => {
                http.get(`http://localhost:${PORT}${req?.query[key]}`, (response) => {
                    if (response.statusCode === 404) {
                        res.write('{'); // wrong start
                        res.write('"error": {'); // error start
                        res.write(`"status": ${response.statusCode},`);
                        res.write('"response": ');

                        response.pipe(res, {end: false});
                    } else {
                        res.write('{ "data": '); // data start
                        response.pipe(res, {end: false});
                    }

                    response.on('end', () => {
                        if (response.statusCode === 404) {
                            res.write('}');
                        }
                        res.write('}');
                        resolve(response);
                    });
                    response.on('error', reject);
                });
            });
            if (i !== keys.length) {
                res.write(',');
            }
        }
        i++;
    }
    res.write('}');
    res.end();
});
/* istanbul ignore next */
router.get('/generate-random-string', async (req, res) => {
    let array = [];
    for (let i = 1; i < 10000; i++) {
        const uniqueRandomId = (Math.random() + 1).toString(36).substring(7);
        const uniqueRandomURI = '/products/' + Math.floor(Math.random() * 1000);
        array.push(`${uniqueRandomId}=${uniqueRandomURI}`);
    }
    for (let i = 1; i < 10000; i++) {
        const uniqueRandomId = (Math.random() + 1).toString(36).substring(7);
        const uniqueRandomURI = '/customers/' + Math.floor(Math.random() * 1000);
        array.push(`${uniqueRandomId}=${uniqueRandomURI}`);
    }
    res.send(`http://localhost:${PORT}/multiple/?${array.join('&')}`);
});

app.use(router);

app.listen(PORT, () => {
});

